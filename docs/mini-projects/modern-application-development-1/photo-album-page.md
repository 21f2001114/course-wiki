# Photo Album Page Generator

## Problem
- You have photos in a directory structure
- You want to upload it to a server along with a album page per folder
- It will use the name of the folder as Album Name
- It will show the thumbnail of each picture when album is opened
- On click of thumbnail, it whill show the full picture in a overlay

## Solution ideas
- Use Jinja and Python
- Read the folder structure
- Create `index.html` in each folder
- Show the images in thumbnail format
- On click show the whole picture
- Show the name of the folder as Album name

## Advanced
- Read exif data, show the camera details, date taken
- Show a map if exif data has geo 
- Generate actual thumbnail images, to reduce the load time